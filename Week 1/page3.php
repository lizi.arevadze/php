<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>3</title>
</head>
<body>
    <h2>ტესტი</h2>
    <form method="POST">
        <ol>
            <li>
                <label for="test1"> ჩამოთვლილთაგან რომელია საქართველოს დედაქალი?</label><br>
                <input type="radio" name="test1" value="თბილისი" required> თბილისი <br>
                <input type="radio" name="test1" value="ქუთაისი"> ქუთაისი <br>
                <input type="radio" name="test1" value="მცხეთა"> მცხეთა <br>
                <input type="radio" name="test1" value="ბათუმი"> ბათუმი <br>
            </li>
            <li>
                <label for="test2"> როდის დაარსდა თბილისი?</label><br>
                <input type="radio" name="test2" value="321 წელს" > 321 წელს <br>
                <input type="radio" name="test2" value="555 წელს"> 555 წელს <br>
                <input type="radio" name="test2" value="455 წელს" required> 455 წელს <br>
                <input type="radio" name="test2" value="678 წელს"> 678 წელს <br>
            </li>
            <li>
                <label for="test3"> რა ერქვა თბილისს მანამდე?</label><br>
                <input type="radio" name="test3" value="ტიფლისი" > ტიფლისი <br>
                <input type="radio" name="test3" value="ტფილისი" required> ტფილისი <br>
                <input type="radio" name="test3" value="ტიფლი"> ტიფლი <br>
                <input type="radio" name="test3" value="ტბილისი"> ტბილისი <br>
            </li>
        </ol>
        <h3>ღია კითხვები</h3>
        <ol start="4">
            <li>
                <label for="open1"> ვინ დაარსა ქალაქი თბილისი?</label><br>
                <input type="text" name="open1" required>
            </li>
            <li>
                <label for="open2"> ვინ გახადა თბილისი ქვეყნის დედაქალაქი?</label><br>
                <input type="text" name="open2" required>
            </li>
        </ol>
        <button type="submit">Submit</button>
    </form>
    
    <?php
    if ($_SERVER['REQUEST_METHOD']=="POST") {
        $correct_answers = 0;

        if ($_POST['test1'] == 'თბილისი') $correct_answers++;
        if ($_POST['test2'] == '455 წელს') $correct_answers++;
        if ($_POST['test3'] == 'ტფილისი') $correct_answers++;

        if ($_POST['open1'] == 'ვახტანგ გორგასალმა') $correct_answers++;
        if ($_POST['open2'] == 'დავით აღმაშენებელმა' || $_POST['open2'] == 'დავით მეოთხემ') $correct_answers++;

        echo "<h3>შედეგი</h3>";
        echo "<p>სწორი პასუხების რაოდენობა: $correct_answers</p>";
    }
    ?>

</body>
</html>