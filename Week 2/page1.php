<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1</title>
</head>
<body>
    <?php
    if (isset($_POST['numberX'])) {
        $numberX = $_POST["numberX"];

        $numericArray = array_fill(0, 12, 0);

        for ($i = 5; $i <= 10; $i++) {
            $numericArray[$i] = rand(10, 100);
        }

        echo "<pre>";
        print_r($numericArray);
        echo "</pre>";

        $countLessThanX = 0;
        $countGreaterThanX = 0;

        foreach ($numericArray as $value) {
            if ($value < $numberX) {
                $countLessThanX++;
            } elseif ($value > $numberX) {
                $countGreaterThanX++;
            }
        }

        echo "Number of elements less than $numberX: $countLessThanX<br>";
        echo "Number of elements greater than $numberX: $countGreaterThanX";
    }
    ?>

    <form method="post">
        <label for="numberX">Enter the number X: </label>
        <input type="number" name="numberX" required>
        <button type="submit">Submit</button>
    </form>
</body>
</html>
