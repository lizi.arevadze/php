<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>2</title>
</head>
<body>
    <?php
    if (isset($_POST['numberX'])) {
        $numberX = $_POST["numberX"];

        $matrix = array(
            array(0, 0, 0, 0),
            array(0, 0, 0, 0),
            array(0, 0, 0, 0),
            array(0, 0, 0, 0)
        );

        for ($i = 1; $i <= 2; $i++) {
            for ($j = 1; $j <= 2; $j++) {
                $matrix[$i][$j] = rand(10, 100);
            }
        }

        echo "<h3>Matrix</h3>";
        echo "<table border='1'>";
        for ($i = 1; $i <= 2; $i++) {
            echo "<tr>";
            for ($j = 1; $j <= 2; $j++) {
                echo "<td>{$matrix[$i][$j]}</td>";
            }
            echo "</tr>";
        }
        echo "</table>";

        echo "<h3>Elements Above Main Diagonal</h3>";
        echo "<table border='1'>";
        for ($i = 1; $i <= 2; $i++) {
            echo "<tr>";
            for ($j = 1; $j < $i; $j++) {
                echo "<td>{$matrix[$i][$j]}</td>";
            }
            echo "</tr>";
        }
        echo "</table>";

        echo "<h3>Multiples of $numberX in the Matrix</h3>";
        echo "<table border='1'>";
        $product = 1;
        $sum = 0;
        $digitSum = 0;

        for ($i = 1; $i <= 2; $i++) {
            echo "<tr>";
            for ($j = 1; $j <= 2; $j++) {
                $element = $matrix[$i][$j];
                $multiple = $element * $numberX;

                echo "<td>{$multiple}</td>";

                $sum += $element;
                $product *= $element;
                $digitSum += array_sum(str_split($element));
            }
            echo "</tr>";
        }
        echo "</table>";

        $count = 2 * 2; 
        $mean = $sum / $count;

        echo "<h3>Additional Calculations</h3>";
        echo "<p>Sum of Matrix Elements: $sum</p>";
        echo "<p>Product of Matrix Elements: $product</p>";
        echo "<p>Arithmetic Mean of Matrix Elements: $mean</p>";
        echo "<p>Sum of Digits of Each Element in Tabular Form:</p>";
        echo "<table border='1'>";
        for ($i = 1; $i <= 2; $i++) {
            echo "<tr>";
            for ($j = 1; $j <= 2; $j++) {
                $element = $matrix[$i][$j];
                $digitSum = array_sum(str_split($element));
                echo "<td>{$digitSum}</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    ?>

    <form method="post">
        <label for="numberX">Enter the number X: </label>
        <input type="number" name="numberX" required>
        <button type="submit">Submit</button>
    </form>
</body>
</html>
