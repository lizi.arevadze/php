<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>3</title>
</head>
<body>
    <?php
    $matrix = array();

    for ($i = 0; $i < 6; $i++) {
        for ($j = 0; $j < 5; $j++) {
            $matrix[$i][$j] = $i + $j;
        }
    }

    echo "<h3>Numeric Matrix with Sum </h3>";
    echo "<table border='1'>";

    for ($i = 0; $i < 6; $i++) {
        echo "<tr>";
        for ($j = 0; $j < 5; $j++) {
            echo "<td>{$matrix[$i][$j]}</td>";
        }
        echo "</tr>";
    }

    echo "</table>";
    ?>
</body>
</html>
