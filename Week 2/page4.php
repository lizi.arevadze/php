<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Car Information</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
    <?php
    $cars = array (
        array(
            "Make" => "Toyota",
            "Model" => "Corolla",
            "Color" => "White",
            "Mileage" => 24000,
            "Status" => "Sold"
        ),

        array(
            "Make" => "Toyota",
            "Model" => "Camry",
            "Color" => "Black",
            "Mileage" => 56000,
            "Status" => "Available"
        ),

        array(
            "Make" => "Honda",
            "Model" => "Accord",
            "Color" => "White",
            "Mileage" => 15000,
            "Status" => "Sold"
        )
    );

    echo "<h3>Car Information</h3>";
    echo "<table>";

    echo "<tr>";
    foreach ($cars[0] as $key => $value) {
        echo "<th>{$key}</th>";
    }
    echo "</tr>";

    foreach ($cars as $car) {
        echo "<tr>";
        foreach ($car as $value) {
            echo "<td>{$value}</td>";
        }
        echo "</tr>";
    }

    echo "</table>";
    ?>
</body>
</html>
