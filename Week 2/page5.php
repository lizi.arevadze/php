<!DOCTYPE html>
<html>
<head>
    <style>
        .error {color: #FF0001;}
    </style>
</head>
<body>

<?php
$nameErr = $emailErr = $websiteErr = $commentErr = $genderErr = "";
$name = $email = $website = $comment = $gender = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = validateInput($_POST["name"], "Name is required");
    $email = validateInput($_POST["email"], "Email is required", "Invalid email format");
    $website = validateInput($_POST["website"], "", "Invalid URL", "", true);
    $comment = validateInput($_POST["comment"], "", "", "", true);
    $gender = validateInput($_POST["gender"], "Gender is required");

    if (empty($nameErr) && empty($emailErr) && empty($websiteErr) && empty($commentErr) && empty($genderErr)) {
        echo "<h3 style='color: green;'>You have successfully registered.</h3>";
        echo "<h2>Your Input:</h2>";
        echo "Name: $name <br>";
        echo "Email: $email <br>";
        echo "Website: $website <br>";
        echo "Comment: $comment <br>";
        echo "Gender: $gender <br>";
    } else {
        echo "<h3 style='color: red;'>You didn't fill out the form correctly.</h3>";
    }
}

function validateInput($data, $requiredMsg = "", $formatErrorMsg = "", $lengthErrorMsg = "", $optional = false) {
    global ${$data . "Err"};
    $value = trim($data);

    if (!$optional || ($optional && !empty($value))) {
        if (empty($value)) {
            ${$data . "Err"} = $requiredMsg;
        } elseif ($data == "email" && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            ${$data . "Err"} = $formatErrorMsg;
        } elseif ($data == "website" && !empty($value) && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $value)) {
            ${$data . "Err"} = $formatErrorMsg;
        } elseif ($data == "comment" && strlen($value) > 100) {
            ${$data . "Err"} = "Comment should be less than 100 characters.";
        } elseif ($data == "gender" && !in_array($value, ["male", "female", "other"])) {
            ${$data . "Err"} = "Invalid gender selected.";
        }
    }

    return $value;
}
?>

<h2>Registration Form</h2>
<span class="error">* required field </span>
<br><br>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    Name:
    <input type="text" name="name" value="<?php echo $name; ?>">
    <span class="error">* <?php echo $nameErr; ?> </span>
    <br><br>
    E-mail:
    <input type="text" name="email" value="<?php echo $email; ?>">
    <span class="error">* <?php echo $emailErr; ?> </span>
    <br><br>
    Website:
    <input type="text" name="website" value="<?php echo $website; ?>">
    <span class="error"><?php echo $websiteErr; ?> </span>
    <br><br>
    Comment:
    <textarea name="comment" rows="4" cols="50"><?php echo $comment; ?></textarea>
    <span class="error"><?php echo $commentErr; ?> </span>
    <br><br>
    Gender:
    <input type="radio" name="gender" value="male" <?php echo ($gender == "male") ? "checked" : ""; ?>> Male
    <input type="radio" name="gender" value="female" <?php echo ($gender == "female") ? "checked" : ""; ?>> Female
    <input type="radio" name="gender" value="other" <?php echo ($gender == "other") ? "checked" : ""; ?>> Other
    <span class="error">* <?php echo $genderErr; ?> </span>
    <br><br>
    <input type="submit" name="submit" value="Submit">
    <br><br>
</form>

<?php
if (empty($nameErr) && empty($emailErr) && empty($genderErr) && $_SERVER["REQUEST_METHOD"] == "POST") {
    echo "<h2>Your Input</h2>";
    echo "<table>";
    echo "<tr><th>Name</th><th>Email</th><th>Gender</th></tr>";
    echo "<tr><td>{$name}</td><td>{$email}</td><td>{$gender}</td></tr>";
    echo "</table>";
}
?>
</body>
</html>
