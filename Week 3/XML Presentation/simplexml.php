<?php
// XML data as a string
$xmlString = '<person><name>John Doe</name><age>30</age></person>';

// Load XML from the string into a SimpleXML object
$xml = simplexml_load_string($xmlString);

// Accessing elements and displaying information
echo 'Name: ' . $xml->name . '<br>';
echo 'Age: ' . $xml->age;
?>