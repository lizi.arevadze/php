<?php
  $dir_error="";
  $FILE_error="";
  if(isset($_POST['c_folder']) && !empty($_POST['c_folder'])){
    $folderName = $_POST["c_folder"];
    $path = "root/" . $folderName;
  
    if( !is_dir($path)){
        mkdir($path);
    }else{
        $dir_error="error";
    }
}
if(isset($_POST['c_file']) && !empty($_POST['c_file'])){
    $folderName = $_POST["c_file"];
    $path = "root/" . $folderName.".txt";
  
    if( !file_exists($path)){
        $file = fopen($path, "w");
    }else{
        $FILE_error="error";
    }

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>lecture 4</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <form action="" method="post">
            <div>
                <input type="text" name="c_folder" placeholder="create folder">
                <button>create folder</button> <span><?=$dir_error?></span>
            </div>
            <div>
                <input type="text" name="c_file" placeholder="create file"> 
                <button>create file</button> <span><?=$FILE_error?></span>
            </div>
        </form>
    </header>

    <main>
        <form method="post">
            
        <?php
            $folderPath = "root/";
            $folders = opendir($folderPath);
            $index=0;
            while (true == ( $read = readdir($folders))) {
                if($read != "." && $read != "" && $read != ".."){

                echo "<input name='dels[]'  value='$read'><button  name='delete' value='$index'>delete</button>";

                if(substr($read,-3) == "txt"){
                    echo "<button  name='edit[]' value='$index'>edit</button>";
                }
                echo "<br>";
                $index++;
                }
            }
            if(isset($_POST['edit']) ){
                echo "<br>";
                $b=$_POST['dels'][$_POST['edit'][0]];
                $path="root/".$b;
                $file = fopen( $path,"r");

                $fileRead = fgets($file);
                fclose($file);

            ?>
                
                <label>text goes here for <?= $b?> file</label>;
                <br>
                <textarea name="finfishedText"cols="30" rows="10"><?=$fileRead?></textarea>;
                <button name="finish" value='<?php echo $b; ?>'>finish editing</button>

            <?php
            }
            ?>
       </form>
    </main>

    <?php
    if(isset($_POST['dels'])){
        if(isset($_POST['delete'])==true){
            if(substr($_POST['dels'][$_POST['delete']], -3)!=="txt"){
                rmdir("root/".$_POST['dels'][$_POST['delete']]);
            }else{
                unlink("root/".$_POST['dels'][$_POST['delete']]);
            }
            header("Location: ".$_SERVER['PHP_SELF']);
            exit();
            
        }
       
    }
    if(isset($_POST['finish'])){
        $finshed = $_POST['finfishedText'];
        $path="root/".$_POST['finish'];
        $file = fopen( $path,"w");
        fwrite($file, $_POST['finfishedText']);
        fclose($file);

    }
    ?>

</body>
</html>